

/////////////////// LED
#include <Wire.h>
#include <Adafruit_GFX.h>
#include "Adafruit_LEDBackpack.h"

Adafruit_BicolorMatrix matrix = Adafruit_BicolorMatrix();


#include <SPI.h>

/******************* Declare Pins */

const int diff_pressure_cs_pin = 12;
const int diff_pressure_rdy_pin = 14; 
const int diff_pressure_sck_pin = 15;

/******************************************************************************
* Typedefs
*******************************************************************************/
typedef enum 
{
  NOT_RDY = 0,
  OVH,
  OVL,
  OK
} status_t;

SPISettings mySettings(5000000, MSBFIRST, SPI_MODE1);
#define DUMMY_BYTE  0x00
/******************************************************************************
* Function Definitions
*******************************************************************************/

/**
 * @brief <h3> CS High </h3>
 *
 * @par
 * Used by HW layer functions to set CS PIN high ( deselect )
 */
void diff_pressure_hal_cs_high()
{
    digitalWrite(diff_pressure_cs_pin, HIGH);
}

/**
 * @brief <h3> CS Low </h3>
 *
 * @par
 * Used by HW layer functions to set CS PIN low ( selecet )
 */
void diff_pressure_hal_cs_low()
{
    digitalWrite(diff_pressure_cs_pin, LOW);
}

/**
 * @brief <h3> HAL Read </h3>
 *
 * @par
 * Reads data from SPI bus
 *
 *
 * @param[out] buffer
 * @param[in] count
 */
uint8_t diff_pressure_hal_read( uint8_t *buffer,
                         uint16_t count )
{
        uint8_t timer = 0;
        
        diff_pressure_hal_cs_low();
        delayMicroseconds(100);
        while( digitalRead(diff_pressure_rdy_pin ))        // SDO pin works as a "ready" pin for data transfering
        {
                diff_pressure_hal_cs_high();
                delayMicroseconds(1);
                diff_pressure_hal_cs_low();
                timer++;
                delay(10);

                if( timer >= 20 )
                        return 0;
        }

        diff_pressure_hal_cs_low();
        while( count-- )
            *( buffer++ ) = SPI.transfer(DUMMY_BYTE);
        diff_pressure_hal_cs_high();

        return -1;
}

/**
 * @brief <h3> Read ADC </h3>
 *
 * @par
 * Reads the ADC value from click without modifying it.
 *
 * @param[out] status of adc reading
 * @param[in] buffer - data will be placed in buffer
 *
 */
 status_t diff_pressure_read_adc( int32_t *buffer )
{
        uint8_t big_buffer[3] = { 0 };
        int32_t retval = 0;

        if( !diff_pressure_hal_read( big_buffer, 3 ) )
                return NOT_RDY;

        if( big_buffer[0] == 0x60 && big_buffer[2] > 0 )
                return OVH;
        if( big_buffer[0] == 0x9F && big_buffer[2] > 0 )
                return OVL;

        retval = big_buffer[0];
        retval = (retval << 8) | big_buffer[1];
        retval = (retval << 8) | big_buffer[2];

    if ( ( big_buffer[0] & 0x20) >> 5 )       // Bit 21 = 1 ; value is negative
        retval = retval - 4194304;            // for 22 bit resolution

        *buffer = retval;
        return OK;
}

/**
 * @brief <h3> Get KPA Difference </h3>
 *
 * @par
 * Returns KPA difference between two pressures
 *
 * @param[out] kpa difference between two pressures
 * @param[in] difference - most recent adc reading
 *
 */
float diff_pressure_get_kpa_difference( int32_t difference )
{
        float press;
        float volt;

        volt = (5./2097151.) * difference;
        press = ( volt / 0.45 ) - 0.45;

        return press;
}

//digitalWrite(diff_pressure_cs_pin, LOW);
//digitalWrite(diff_pressure_cs_pin, HIGH);


//////////////////////////////////////////// Bluetooth settings start
#include <Arduino.h>
#include <SPI.h>
#if not defined (_VARIANT_ARDUINO_DUE_X_) && not defined (_VARIANT_ARDUINO_ZERO_)
  #include <SoftwareSerial.h>
#endif

#include "Adafruit_BLE.h"
#include "Adafruit_BluefruitLE_SPI.h"
#include "Adafruit_BluefruitLE_UART.h"

#include "BluefruitConfig.h"
/////////////////////////////////////////////
/*-----------------------------------------------------------------------*/
    #define FACTORYRESET_ENABLE         1
    #define MINIMUM_FIRMWARE_VERSION    "0.6.6"
    #define MODE_LED_BEHAVIOUR          "MODE"
    #define DEVICE_NAME                 "Breathing Games"

/*=========================================================================*/

Adafruit_BluefruitLE_SPI ble(BLUEFRUIT_SPI_CS, BLUEFRUIT_SPI_IRQ, BLUEFRUIT_SPI_RST);

// A small helper
void error(const __FlashStringHelper*err) {
  Serial.println(err);
  while (1);
}
//////////////////////////////////////////// Bluetooth settings end

void setup() {
  // put your setup code here, to run once:
  Serial.begin(115200);                     // Initialise UART

/********************************* Bluetooth *************************************************/
  if ( !ble.begin(VERBOSE_MODE) )
  {
    error(F("Couldn't find Bluefruit, make sure it's in CoMmanD mode & check wiring?"));
  }
  Serial.println( F("OK!") );

  if ( FACTORYRESET_ENABLE )
  {
    /* Perform a factory reset to make sure everything is in a known state */
    Serial.println(F("Performing a factory reset: "));
    if ( ! ble.factoryReset() ){
      error(F("Couldn't factory reset"));
    }
  }

  /* Disable command echo from Bluefruit */
  ble.echo(false);

  Serial.println("Requesting Bluefruit info:");
  /* Print Bluefruit information */
  ble.info();


  ble.verbose(false);  // debug info is a little annoying after this point!

////////////////////// Connection
//  /* Wait for connection */
//  while (! ble.isConnected()) {
//      delay(500);
//  }

  // LED Activity command is only supported from 0.6.6
  if ( ble.isVersionAtLeast(MINIMUM_FIRMWARE_VERSION) )
  {
    // Change Mode LED Activity
    Serial.println(F("******************************"));
    Serial.println(F("Change LED activity to " MODE_LED_BEHAVIOUR));
    ble.sendCommandCheckOK("AT+HWModeLED=" MODE_LED_BEHAVIOUR);
    ble.sendCommandCheckOK("AT+GAPDEVNAME=" DEVICE_NAME);
  }


/*********************************************************************************************/

/*********************************************** SPI ******************************/
  pinMode(diff_pressure_cs_pin, OUTPUT);  // Set Different pressure CS pin as output
  pinMode(diff_pressure_sck_pin, OUTPUT);
  pinMode(diff_pressure_rdy_pin, INPUT);
  SPI.beginTransaction(mySettings);
  digitalWrite(diff_pressure_cs_pin, LOW);
  //digitalWrite(diff_pressure_cs_pin, HIGH);
  SPI.begin();
/***********************************************************************************/

matrix.begin(0x70);  // pass in the address


}

static const uint8_t PROGMEM
  smile_bmp[] =
  { B00111100,
    B01000010,
    B10100101,
    B10000001,
    B10100101,
    B10011001,
    B01000010,
    B00111100 },
  neutral_bmp[] =
  { B00111100,
    B01000010,
    B10100101,
    B10000001,
    B10111101,
    B10000001,
    B01000010,
    B00111100 },
  frown_bmp[] =
  { B00111100,
    B01000010,
    B10100101,
    B10000001,
    B10011001,
    B10100101,
    B01000010,
    B00111100 };

boolean rdy_state;
uint8_t timer = 0;

int32_t adc_buffer = 0;
float difference = 0.;

int line_value = 0;

int i,j;

void get_diff_readings_line_value(){
  if ( diff_pressure_read_adc( &adc_buffer) == OK){
    //Serial.print("ADC: ");
    //Serial.println(adc_buffer);
    line_value = map(adc_buffer,39000, 1040138, 0, 7);
    difference = diff_pressure_get_kpa_difference(adc_buffer);
    Serial.print("KPA difference: ");
    Serial.println(difference, 4);
    ble.print("AT+BLEUARTTX=");
    ble.println(difference);
    adc_buffer = 0;
  }else if( diff_pressure_read_adc( &adc_buffer) == OVH) Serial.println("Overflow happened");
  else if ( diff_pressure_read_adc( &adc_buffer) == OVL) Serial.println("Underflow happened"); 
}

unsigned int long last_print_time = 0;

#define LOOP_BAR_DELAY 250

void loop() {

  //Serial.println("New cycle");
  //delay(1000);



// For examples go to :
// File > Examples > ADAFRUIT LED Backpack Library > bicolor8x8

    matrix.clear();


  for (j=0; j<=7; j++){
    do{
      get_diff_readings_line_value();
      matrix.drawLine(j,7,j,7,LED_GREEN);
      matrix.drawLine(j,7-line_value,j,7, LED_YELLOW);
      matrix.writeDisplay();  // write the changes we just made to the display
           
    }while(millis() - last_print_time <= (LOOP_BAR_DELAY + LOOP_BAR_DELAY * j));
  }

  last_print_time = millis();
  

}

