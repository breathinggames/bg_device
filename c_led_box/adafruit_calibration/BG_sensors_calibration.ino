/* 
* Hardware hookup 
* Sensor     Arduino Pin
* Ground     Ground
* +10-12V      Vin
* Out          A1
* TMP          A0
*/

/*
For reading serial in Ubuntu, enter in shell
sudo systemctl stop ModemManager.service

once finished working start service
sudo systemctl start ModemManager.service

*/

//////////////////////////////////////////// Bluetooth settings start
//#include <Arduino.h>
#include <SPI.h>
#include <stdint.h>
#include <stdbool.h>
#include <stddef.h>

typedef enum 
{
  NOT_RDY = 0,
  OVH,
  OVL,
  OK
} status_t;

SPISettings mySettings(5000000, MSBFIRST, SPI_MODE1);
#define DUMMY_BYTE  0x00

#if not defined (_VARIANT_ARDUINO_DUE_X_) && not defined (_VARIANT_ARDUINO_ZERO_)
  #include <SoftwareSerial.h>
#endif

#include "Adafruit_BLE.h"
#include "Adafruit_BluefruitLE_SPI.h"
#include "Adafruit_BluefruitLE_UART.h"

#include "BluefruitConfig.h"
/////////////////////////////////////////////
/*-----------------------------------------------------------------------*/
    #define FACTORYRESET_ENABLE         1
    #define MINIMUM_FIRMWARE_VERSION    "0.6.6"
    #define MODE_LED_BEHAVIOUR          "MODE"
    #define DEVICE_NAME                 "Breathing Games"

/*=========================================================================*/

Adafruit_BluefruitLE_SPI ble(BLUEFRUIT_SPI_CS, BLUEFRUIT_SPI_IRQ, BLUEFRUIT_SPI_RST);

// A small helper
void error(const __FlashStringHelper*err) {
  Serial.println(err);
  while (1);
}
//////////////////////////////////////////// Bluetooth settings end

////////// Pin declaration
const int analogInPin = A0;  // Analog input pin that the potentiometer is attached to
const int analogOutPin = 9; // Analog output pin that the fan PWM is attached to
const int interruptPin = 2; // Interrupt pin where tachometer from the fan is attached to

const int diff_pressure_cs_pin = 12;
const int diff_pressure_rdy_pin = 14; 
const int diff_pressure_sck_pin = 15;

const int OutPin  = A1;   // wind sensor analog pin  hooked up to Wind P sensor "OUT" pin
const int TempPin = A2;   // temp sesnsor analog pin hooked up to Wind P sensor "TMP" pin
const int ShutDownPin = 13; //Pull high to disable heating element for the sensor. Hooked up to  "SHDN" pin

/******************************************************************************
* Typedefs
*******************************************************************************/


/******************************************************************************
* Function Definitions
*******************************************************************************/

/**
 * @brief <h3> CS High </h3>
 *
 * @par
 * Used by HW layer functions to set CS PIN high ( deselect )
 */
void diff_pressure_hal_cs_high()
{
    digitalWrite(diff_pressure_cs_pin, HIGH);
}

/**
 * @brief <h3> CS Low </h3>
 *
 * @par
 * Used by HW layer functions to set CS PIN low ( selecet )
 */
void diff_pressure_hal_cs_low()
{
    digitalWrite(diff_pressure_cs_pin, LOW);
}

/**
 * @brief <h3> HAL Read </h3>
 *
 * @par
 * Reads data from SPI bus
 *
 *
 * @param[out] buffer
 * @param[in] count
 */
uint8_t diff_pressure_hal_read( uint8_t *buffer,
                         uint16_t count )
{
        uint8_t timer = 0;
        
        diff_pressure_hal_cs_low();
        delayMicroseconds(100);
        while( digitalRead(diff_pressure_rdy_pin ))        // SDO pin works as a "ready" pin for data transfering
        {
                diff_pressure_hal_cs_high();
                delayMicroseconds(1);
                diff_pressure_hal_cs_low();
                timer++;
                delay(10);

                if( timer >= 20 )
                        return 0;
        }

        diff_pressure_hal_cs_low();
        while( count-- )
            *( buffer++ ) = SPI.transfer(DUMMY_BYTE);
        diff_pressure_hal_cs_high();

        return -1;
}

/**
 * @brief <h3> Read ADC </h3>
 *
 * @par
 * Reads the ADC value from click without modifying it.
 *
 * @param[out] status of adc reading
 * @param[in] buffer - data will be placed in buffer
 *
 */
status_t diff_pressure_read_adc( int32_t *buffer )
{
        uint8_t big_buffer[3] = { 0 };
        int32_t retval = 0;

        if( !diff_pressure_hal_read( big_buffer, 3 ) )
                return NOT_RDY;

        if( big_buffer[0] == 0x60 && big_buffer[2] > 0 )
                return OVH;
        if( big_buffer[0] == 0x9F && big_buffer[2] > 0 )
                return OVL;

        retval = big_buffer[0];
        retval = (retval << 8) | big_buffer[1];
        retval = (retval << 8) | big_buffer[2];

    if ( ( big_buffer[0] & 0x20) >> 5 )       // Bit 21 = 1 ; value is negative
        retval = retval - 4194304;            // for 22 bit resolution

        *buffer = retval;
        return OK;
}

/**
 * @brief <h3> Get KPA Difference </h3>
 *
 * @par
 * Returns KPA difference between two pressures
 *
 * @param[out] kpa difference between two pressures
 * @param[in] difference - most recent adc reading
 *
 */
float diff_pressure_get_kpa_difference( int32_t difference )
{
        float press;
        float volt;

        volt = (5./2097151.) * difference;
        press = ( volt / 0.45 ) - 0.45;

        return press;
}

//////// Variable declaration

//Increases the size of global variables big time. Can be be managed in case we run into memory issues
const int number_of_readings = 20; 
int analog_index = 0;

int sensorValue = 0;        // value read from the pot
int outputValue = 0;        // value output to the PWM (analog out)

long rpm_counter = 0;
float duration = 0;
unsigned long last_print_time = 0;

int windADunits;

float wspeed_adc = 0.0;
float wspeed_array[number_of_readings];
float wspeed_total = 0.0;

int tempRawAD;

#define PRINT_DELAY 500

uint8_t timer = 0;

int32_t adc_buffer = 0;
float difference = 0.;

//////////////////////// payload struct
#define SENSOR_NUMBER 6

//Payload order
#define WINDSPEED     0
#define TEMPERATURE   1
#define RPM           2
#define DIFFP_ADC     3
#define DIFFP_KPA     4
#define TEMP_1WIRE    5

typedef struct
{
    float header;
    float sensor_payload[SENSOR_NUMBER];
    char troubles;// Do bit masking! maybe make a union
} node_data;

node_data node; 

void blink(){
  rpm_counter++;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                            Analog smoothing

float smoothing(const int* numb_readings, float* total_sum,
                float* array_item, int* analog_pin){

  *total_sum -= *array_item;
  *array_item = analogRead(*analog_pin);
  *total_sum += *array_item;
  float result = *total_sum / *numb_readings;
  return result;
}


/* rpm_conversion
 * Disable interrupts, calculate duration since 
 */
float rpm_conversion(){
  detachInterrupt(interruptPin);
  duration = millis() - last_print_time;
  float rpm = rpm_counter / (duration / 1000.0) * 60.0;
  rpm_counter = 0;
  rpm /= 2; // Divide by 2, because interrut triggers double.. for some reason (known issue with Arduinos)
  attachInterrupt(digitalPinToInterrupt(interruptPin), blink, FALLING);
  return rpm;
}

void sensor_print(){
  // print the results to the serial monitor:
  Serial.print("RPM = ");
  Serial.print(node.sensor_payload[RPM]);
  Serial.print("\t sensor = ");
  Serial.print(sensorValue);
  Serial.print("\t output = ");
  Serial.println(outputValue);
}

void print_struct(){
  int i;
  for(i=0;i<SENSOR_NUMBER;i++){
    if(i == DIFFP_KPA){
      Serial.print(node.sensor_payload[i], 6);
      Serial.print(";");
      
    }else{
    Serial.print(node.sensor_payload[i]);
    Serial.print(";");
    }
  }
    Serial.println(millis());
}

void read_analog_write_PWM(){
  // read the analog in value:
  sensorValue = analogRead(analogInPin);
  // map it to the range of the analog out:
  outputValue = map(sensorValue, 0, 1023, 0, 255);
  // change the analog out value:
  analogWrite(analogOutPin, outputValue);  
}

void adc_read_and_smoothings(){
  // Add all analog sensors that required smoothing here
  wspeed_adc = smoothing(&number_of_readings, &wspeed_total,
                         &wspeed_array[analog_index], &OutPin);

  tempRawAD = analogRead(TempPin);
    
  analog_index++;
  if (analog_index == number_of_readings) analog_index = 0;
                      
}

float wind_speed_conversion(){
  float windMPH =  pow((((float)wspeed_adc - 400.0) / 85.6814), 3.36814);
  float windmps = windMPH * 0.44704;
  return windmps;
                         
}

float tempC_conversion(){
  // convert to volts then use formula from datatsheet 
  // Vout = ( TempC * .0195 ) + .400
  // tempC = (Vout - V0c) / TC   see the MCP9701 datasheet for V0c and TC
  float tempC;
  tempC = ((((float)tempRawAD * 3.3) / 1024.0) - 0.400) / .0195;
  return tempC;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                            8. TEMPERATURE SENSORS
#include <OneWire.h>
#include <DallasTemperature.h>


#define ONE_WIRE_BUS 6                // Data wire is plugged into port 2 on the Arduino
#define TEMPERATURE_PRECISION 9        // 9 bits or one decimal point

// Setup a oneWire instance to communicate with any OneWire devices (not just Maxim/Dallas temperature ICs)
OneWire oneWire(ONE_WIRE_BUS);        

DallasTemperature sensors(&oneWire);                  // Pass our oneWire reference to Dallas Temperature.
DeviceAddress casingTemperature, ambientTemperature, glandTemperature;   // OneWire variables
unsigned long lastTempRequest = 0;
int  delayInMillis = 0;
float ambient_temperature = 0.0;
int  idle = 0;

void one_wire_temps(){
  // OneWire sensor setup
    // Need to have 750ms for 
    if(millis() - lastTempRequest >= 750){
      sensors.requestTemperatures();
      ambient_temperature = sensors.getTempCByIndex(0);
      node.sensor_payload[TEMP_1WIRE] = ambient_temperature;
      lastTempRequest = millis();     
    }

}


void setup() {
  // initialize serial communications at 9600 bps:
  Serial.begin(9600);

  /*********************************************** SPI ******************************/
  pinMode(diff_pressure_cs_pin, OUTPUT);  // Set Different pressure CS pin as output
  pinMode(diff_pressure_sck_pin, OUTPUT);
  pinMode(diff_pressure_rdy_pin, INPUT);
  SPI.beginTransaction(mySettings);
  digitalWrite(diff_pressure_cs_pin, LOW);
  //digitalWrite(diff_pressure_cs_pin, HIGH);
  SPI.begin();
/************************************************* end SPI **********************************/
  
  pinMode(interruptPin, INPUT_PULLUP);
  attachInterrupt(digitalPinToInterrupt(interruptPin), blink, FALLING);

  pinMode(ShutDownPin, OUTPUT);

  analogWrite(analogOutPin, 0);


  
///********************************* Bluetooth *************************************************/
//  if ( !ble.begin(VERBOSE_MODE) )
//  {
//    error(F("Couldn't find Bluefruit, make sure it's in CoMmanD mode & check wiring?"));
//  }
//  Serial.println( F("OK!") );
//
//  if ( FACTORYRESET_ENABLE )
//  {
//    /* Perform a factory reset to make sure everything is in a known state */
//    Serial.println(F("Performing a factory reset: "));
//    if ( ! ble.factoryReset() ){
//      error(F("Couldn't factory reset"));
//    }
//  }
//
//  /* Disable command echo from Bluefruit */
//  ble.echo(false);
//
//  Serial.println("Requesting Bluefruit info:");
//  /* Print Bluefruit information */
//  ble.info();
//
//
//  ble.verbose(false);  // debug info is a little annoying after this point!
//
//////////////////////// Connection
////  /* Wait for connection */
////  while (! ble.isConnected()) {
////      delay(500);
////  }
//
//  // LED Activity command is only supported from 0.6.6
//  if ( ble.isVersionAtLeast(MINIMUM_FIRMWARE_VERSION) )
//  {
//    // Change Mode LED Activity
//    Serial.println(F("******************************"));
//    Serial.println(F("Change LED activity to " MODE_LED_BEHAVIOUR));
//    ble.sendCommandCheckOK("AT+HWModeLED=" MODE_LED_BEHAVIOUR);
//    ble.sendCommandCheckOK("AT+GAPDEVNAME=" DEVICE_NAME);
//  }
//
//
///************************************************** BlueTooth ends**************************************/
// OneWire sensor setup
  sensors.begin();
  if (!sensors.getAddress(ambientTemperature, 0)) Serial.println("Unable to find address for Device 0"); 
  sensors.setResolution(ambientTemperature, TEMPERATURE_PRECISION);
  sensors.setWaitForConversion(false);      //Important undocumented feature that is only needed for "parasitic mode"
  sensors.requestTemperatures();
  delayInMillis = 750 / (1 << (3)); 
  lastTempRequest = millis(); 


  //Print header here
  while(millis()<5000);
  Serial.println("Breathing Games 0.1;WindSpeed;mps;0-30;Temperature;C;0-37;FanSpeen;rpm;0-17000;Timestamp;");
}

void loop() {

  
  adc_read_and_smoothings();

  read_analog_write_PWM();


  
  if(millis() - last_print_time >= PRINT_DELAY){

    one_wire_temps();
    
    if ( diff_pressure_read_adc( &adc_buffer) == OK){
      //Differential pressure check
      //Serial.print("ADC: ");
      //Serial.println(adc_buffer);

      node.sensor_payload[DIFFP_ADC] = adc_buffer;
      
      difference = diff_pressure_get_kpa_difference(adc_buffer);
      //Serial.print("KPA difference: ");
      //Serial.println(difference, 4);
      node.sensor_payload[DIFFP_KPA] = difference;
      adc_buffer = 0;
      }else if( diff_pressure_read_adc( &adc_buffer) == OVH) Serial.println("Overflow happened");
      else if ( diff_pressure_read_adc( &adc_buffer) == OVL) Serial.println("Underflow happened");
      
    node.sensor_payload[WINDSPEED] = wind_speed_conversion();
    node.sensor_payload[TEMPERATURE] = tempC_conversion();
    node.sensor_payload[RPM] = rpm_conversion();
    
    //sensor_print();
    print_struct();
    
    last_print_time = millis();
    }
  
}



