#include <Adafruit_LPS35HW.h>
#include <BleConnectionStatus.h>
#include <BleGamepad.h>

/* includes ************************************************************************************/ 


/* end includes ********************************************************************************/

/* constant defines ****************************************************************************/

/* Hardware IO */
#define BUTTON_1_PIN          14
#define BUTTON_2_PIN          15
#define BUTTON_3_PIN          26

#define RED_LED_PIN           16
#define GREEN_LED_PIN         17
#define BLUE_LED_PIN          18

#define BATTERY_VOLTAGE_PIN   35 // Measure Battery voltage 1/2 voltage divider (47k/47K)

#define SENSOR_SDA_PIN        23
#define SENSOR_SCL_PIN        32

#define LPS33HW_A_ADDRESS      0x5c
#define LPS33HW_B_ADDRESS      0x5d




/* Sampling settings */ 
  
 /*
** How often we check for any changes
* 
*/
#define TIMESTEP              20  // pressure sampling rate 50 times per sec

/* Sleep Timeout
 *  Open Gamepad will go to sleep after this time unless any of the  buttons have been pressed
 *  Pressing Start button will then wake and restart
 */
#define SLEEP_TIMEOUT         600000  // 10 minutes

/* Reset Timeout
 *  Holding down the Select and then the Start button for this time will force the Open Gamepad to reset.
 */
#define RESET_TIMEOUT         10000  // 10 seconds

/* 
 *  How often we check the battery Voltage
*
*/
#define BATTERY_INTERVAL             10000  // 10 seconds

/* 
 *  How long we need for accumulate press
*
*/
#define ACCUMULATE_DELAY            2000  // 2 second


#define FLOW_CONSTANT               231.0f

/* Modes */

//#define LOCAL_PRINT 
#define PRESSURES_PLOT
#define UPDATE_PRESSURE_ZERO
/* end constant defines ************************************************************************/

/* variable declaration ************************************************************************/


volatile boolean     accumulate = 0;
volatile boolean     accumulate_pending = false;
volatile boolean     reset_pending = false;
volatile uint32_t    sleepTime          = 0;
volatile uint32_t    resetTime          = 0;
volatile uint32_t    accumulateTime     = 0;


uint16_t   battery_voltage;
uint16_t   old_battery_level, new_battery_level;
//uint8_t    battery_level;

boolean    LPS33HW_sensor_OK = true;

int32_t    last_timestepTime           = 0; 
int32_t    currentTime                 = 0; 

float pressure_A, pressure_B;             // pressure for sensors in pascals
float zero_pressure_A, zero_pressure_B;   // zero pressure value in pascals
float pressure_difference;
float flow_rate;                          // flow rate in ml/sec
boolean    volume_state; // true if we are accumulaing the volume from the flow
float      volume; // volume in ml
                        
int16_t HID_pressure,HID_flow_rate, HID_volume;


BleGamepad SpiroGaming("Spirotroller", "Espressif", 100);
TwoWire  WireSensor = TwoWire(1);
Adafruit_LPS35HW LPS33HW_A = Adafruit_LPS35HW(); 
Adafruit_LPS35HW LPS33HW_B = Adafruit_LPS35HW();

/* end variable declaration ********************************************************************/


void IRAM_ATTR Button_1_isr() {
 
  if(digitalRead(BUTTON_1_PIN) == 0)
  {
    SpiroGaming.press(BUTTON_1);
    if(digitalRead(BUTTON_3_PIN) == 0){
      reset_pending = true;
      resetTime = millis();
    }
  }
  else
  {
    SpiroGaming.release(BUTTON_1);
    reset_pending = false;
  }
  sleepTime = millis();
}

void IRAM_ATTR Button_2_isr() {
  
  if(digitalRead(BUTTON_2_PIN) == 0){
    SpiroGaming.press(BUTTON_2);
    accumulate_pending = true;
    accumulateTime = millis();    
  }
  else
  {
    SpiroGaming.release(BUTTON_2);
    accumulate_pending = false;
    accumulateTime = millis();   
  }
  
  sleepTime = millis();
}

void IRAM_ATTR Button_3_isr() {
  
  if(digitalRead(BUTTON_3_PIN) == 0)
  {
    SpiroGaming.press(BUTTON_3);
    if(digitalRead(BUTTON_1_PIN) == 0){
      reset_pending = true;
      resetTime = millis();
    }
  }
  else
  {
    SpiroGaming.release(BUTTON_3);
    reset_pending = false;
  }
  sleepTime = millis();
}

// Update pressure zero
void update_pressure_zero(void){
  if (pressure_A > 0.0f) zero_pressure_A += 0.01f;
  else if (pressure_A < 0.0f) zero_pressure_A -= 0.01f;

  if (pressure_B > 0.0f) zero_pressure_B += 0.01f;
  else if (pressure_B < 0.0f) zero_pressure_B -= 0.01f;
}

 /**
  * Print output data to serial
  */
void print_readings(void){
  
  
  Serial.print(" Pressure_A ");
  Serial.print(pressure_A);
  Serial.print("Pressure B ");
  Serial.print(pressure_B );
  Serial.print(" Zzzz  ");
  Serial.print((currentTime-sleepTime)/1000);
  Serial.print(" Battery  ");
  Serial.print(new_battery_level);
  Serial.println("%");
}

void sleep_mode(void){
  /* Disable Bluetooth */
//  esp_bt_controller_disable();
  /* Enable RTC pullup on Start button */

  /* Wake on Start Button */
  esp_sleep_enable_ext0_wakeup(GPIO_NUM_14, LOW);

  /* Zzzzzz */
  esp_deep_sleep_start();
  
}

void check_battery(void){
  battery_voltage = map(analogRead(BATTERY_VOLTAGE_PIN), 0, 4095, 0, 7100); // battery voltage in units of 10mV
  new_battery_level = (uint8_t) map(battery_voltage, 3200, 4200, 0, 100);
  if(new_battery_level != old_battery_level) {
    SpiroGaming.setBatteryLevel(new_battery_level);
    old_battery_level = new_battery_level;
  }
  
}

void setup() {

  Serial.begin(115200);

   
  pinMode(RED_LED_PIN, OUTPUT);
  pinMode(GREEN_LED_PIN, OUTPUT);
  pinMode(BLUE_LED_PIN, OUTPUT); 
  digitalWrite(RED_LED_PIN, HIGH);
  digitalWrite(GREEN_LED_PIN, LOW);
  digitalWrite(BLUE_LED_PIN, HIGH);
  
  pinMode(BUTTON_1_PIN, INPUT_PULLUP);
  pinMode(BUTTON_2_PIN, INPUT_PULLUP);
  pinMode(BUTTON_3_PIN, INPUT_PULLUP);

   // Interrupts for the buttons
  attachInterrupt(BUTTON_1_PIN, Button_1_isr, CHANGE);
  attachInterrupt(BUTTON_2_PIN, Button_2_isr, CHANGE);
  attachInterrupt(BUTTON_3_PIN, Button_3_isr, CHANGE);

   // Set up the LPS33HW pressure Sensors
  WireSensor.begin (SENSOR_SDA_PIN, SENSOR_SCL_PIN);   // 
  
  if (!LPS33HW_A.begin_I2C(LPS33HW_A_ADDRESS, &WireSensor)) {
    Serial.println(" LPS33HW Sensor A fail");
    LPS33HW_sensor_OK = false;
  }
  if (!LPS33HW_B.begin_I2C(LPS33HW_B_ADDRESS, &WireSensor)) {
    Serial.println(" LPS33HW Sensor B fail");
    LPS33HW_sensor_OK = false;
  }

  // Initialise the zero
  
  
  for (int i =0; i < 100; i++){
    zero_pressure_A += LPS33HW_A.readPressure();
    zero_pressure_B += LPS33HW_B.readPressure();
    delay(10);
  }
  
  // One shot measurement mode 
  LPS33HW_A.setDataRate(LPS35HW_RATE_ONE_SHOT);
  LPS33HW_B.setDataRate(LPS35HW_RATE_ONE_SHOT);
  
  // Start the bluetooth

  SpiroGaming.begin();
  SpiroGaming.setAutoReport(false);

  sleepTime = millis();
}

void loop() {
 
  currentTime = millis();
  if(currentTime-last_timestepTime > TIMESTEP){
     if(reset_pending && (currentTime-resetTime > RESET_TIMEOUT)){
        ESP.restart();
     }

     if(accumulate_pending && (currentTime-accumulateTime > ACCUMULATE_DELAY)){
        accumulate_pending = false;
        if(volume_state == false) {
          volume_state = true;         
          digitalWrite(RED_LED_PIN, LOW);
          SpiroGaming.press(BUTTON_4);
         
          volume = 0;
        }    
       else {
          volume_state = false;         
          digitalWrite(RED_LED_PIN, HIGH);
          SpiroGaming.release(BUTTON_4);
          

       }
     }
     
     if(currentTime-sleepTime > SLEEP_TIMEOUT) sleep_mode();
     
     check_battery();
     
     pressure_A = LPS33HW_A.readPressure() * 100; // pressure in pa
     pressure_A -= zero_pressure_A;
     pressure_A = constrain(pressure_A, -10000, 10000); // limit to +/- 10kpa

     pressure_B = LPS33HW_B.readPressure() * 100; // pressure in pa
     pressure_B -= zero_pressure_B;
     pressure_B = constrain(pressure_B, -10000, 10000); // limit to +/- 10kpa
     
     pressure_difference = pressure_A - pressure_B; // get the differential venturi pressure
     pressure_difference = abs(pressure_difference);  // must be +ve for sqrt

     if(pressure_A > 0.0f) flow_rate = FLOW_CONSTANT * sqrt(pressure_difference); // Exhale +ve ml/s
     else flow_rate = -FLOW_CONSTANT * sqrt(pressure_difference); // Inhale -ve ml/s
     flow_rate = constrain(flow_rate, -10000, 10000);

     if(volume_state == true)volume += flow_rate * TIMESTEP / 1000; //volume in ml
     else volume = 0;
     volume =  constrain(volume, -10000, 10000);

     LPS33HW_A.takeMeasurement();
     LPS33HW_B.takeMeasurement();
     
     HID_pressure = (int16_t) map(pressure_A, -10000, 10000, -32767, 32767);
     HID_flow_rate = (int16_t) map(flow_rate, -10000, 10000, -32767, 32767);
     HID_volume = (int16_t) map(volume, -10000, 10000, -32767, 32767);
  
#ifdef LOCAL_PRINT
    print_readings();
#endif

#ifdef PRESSURES_PLOT
    Serial.print(flow_rate);
    Serial.print(",");
    Serial.print(volume);
    Serial.print(",");
    Serial.println(pressure_A);
#endif

#ifdef UPDATE_PRESSURE_ZERO
    update_pressure_zero();
#endif

    if(SpiroGaming.isConnected()) {
      digitalWrite(BLUE_LED_PIN, LOW);
     
     SpiroGaming.setAxes(0, 0,HID_flow_rate, HID_pressure, 0, 0, HID_volume, 0, DPAD_CENTERED, HAT_CENTERED, HAT_CENTERED, HAT_CENTERED);
     SpiroGaming.sendReport();
    }
    else {
      digitalWrite(BLUE_LED_PIN, HIGH);
    }
    last_timestepTime = currentTime;
  }



}
