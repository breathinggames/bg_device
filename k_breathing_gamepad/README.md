# Breathing Gamepad
The Breathing Gamepad is a combination of a breath controller and a handheld Gamepad similar to the Sony PS4 Dualshock controller. At present this is a development device to develop the best physical form factor and to integrate into Unity based games and data visualisation

![Image Breathing Gamepad](Docs/breathing_gamepad.jpg)

## A Breathinggames device that looks and feels like a Game Controller
- Similar shape and layout of buttons and joysticks to common Playstation and XBox controllers
- Breath measurement devices build into the game controller

## Removable "noses" with different breath sensors
- Alternative breath measurement methods (Pressure, Turbine, Ultrasonic)
- Breath sensor is removable to make sterile by sterilisation or being disposable.

Alongside the Spirotroller gaming, the Breathing Gamepad also is a development platform for the following evolutionary improvements to Breathinggames devices
### Bluetooth Low Energy and Unity Integration
- Emulate BLE Gamepad
- Communicate with games through Unity Input Manager/System
- Cross platform support including Web based games

### More options for measurements
- Dual pressure sensors for flow rate, direction, and pressure
- Optional restrictor add-on to create back pressure

This version draws from the developments of the Spirotroller.

### Evolutionary Approach
This version is intended to be a development platform for the following activities:
- Creation of a physical package solution which is tactile, aesthetic, and effective.
- Update existing and create new Unity based games
- Data visualisation in Unity for breath data
- New Therapies and measurements
- Medical device compliance and sterilisation
- Improved packaging for aesthetic and usage
- Gaming performance evaluation
- Hardware improvements, cost reduction and easy assembly

## Resources

### [Operation Guide](Docs/OpGuide.md)

### [Firmware and Software Development](Docs/Firmware.md)

### [CAD design](Docs/CAD.md)

### [Battery Installation Guide](Docs/Battery_Guide.md)

## Contributors
Contributors are welcome. To join, contact info (at) breathinggames.net

- Jim Anastassio
- Matthew Brown
- Tiberius Brastaviceanu
- Richard Ibbotson
- Povilas Jurgaitis
- Emmanuel Kellner
- Humberto Quintana
- Andres Romero Vasquez
- Kevin Yang



