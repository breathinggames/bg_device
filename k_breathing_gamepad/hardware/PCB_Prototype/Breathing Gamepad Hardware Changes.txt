Breathing Gamepad Hardware Changes

Replace transistors to UMH3N with integral base resistor
Change power supply so USB to Serial is not taking power from battery, only USB
Change CP2104 to CP2102N due to obsolescence
Add voltage divider to Vbus
Add SP0503 protection device
Add second voltage regulator for IO +3.3VP to reduce power
Add two more LEDs
Replace Start and select buttons with PS, Share, and options buttons
Joysticks are now RKJXV1224005
Add component values for Rs and Cs


