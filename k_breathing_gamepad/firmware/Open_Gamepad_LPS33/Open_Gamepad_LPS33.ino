

/*
 * Spirotroller.ino
 * This program is for the Sprirotroller Enhanced with ESP32 contolller
 * It appears as a BLE Gamepad
 * 
 * 
 */
/* includes ************************************************************************************/ 
#include <Adafruit_LPS35HW.h>
#include <Adafruit_MCP23X17.h>
#include <BleConnectionStatus.h>
#include <BleGamepad.h>


/* end includes ********************************************************************************/

/* constant defines ****************************************************************************/

/* Hardware IO */
#define START_BUTTON_PIN      15
#define SELECT_BUTTON_PIN     16

// Physical Button bits in MCP_buttons_state
#define MCP_NUM_BUTTONS       16
#define MCP_SHOULDER_L        0x01
#define MCP_SHOULDER_R        0x02
#define MCP_JOYSTICK_L        0x04
#define MCP_JOYSTICK_R        0x08

#define MCP_DPAD_N            0x100
#define MCP_DPAD_E            0x200
#define MCP_DPAD_S            0x400
#define MCP_DPAD_W            0x800
#define MCP_BUTTON_A          0x2000
#define MCP_BUTTON_B          0x1000
#define MCP_BUTTON_X          0x8000
#define MCP_BUTTON_Y          0x4000


#define STATUS_LED_PIN        22

#define BATTERY_INPUT_PIN     35 // 35 is on ADC1 so works when bluetooth enabled
#define JOYSTICK_L_V_PIN      36 
#define JOYSTICK_L_H_PIN      39           
#define JOYSTICK_R_V_PIN      33 
#define JOYSTICK_R_H_PIN      34

#define NOSE_PIN_1            18  
#define NOSE_PIN_2                    // 3.3V
#define NOSE_PIN_3            19
#define NOSE_PIN_4            17
#define NOSE_PIN_5            14
#define NOSE_PIN_6            25
#define NOSE_PIN_7            32      // ADC capable 
#define NOSE_PIN_8                    // Ground

#define MCP23017_SDA_PIN      23
#define MCP23017_SCL_PIN      13
#define MCP23017_RESET_PIN    26
#define MCP23017_INTA_PIN     27
#define MCP23017_I2C_ADDR     0x20

#define ADC_STEPS             4095
#define ADC_REFERENCE         3.3    // Volts


/* Sampling settings */
#define TIMESTEP              20

/* Sleep Timeout
 *  Open Gamepad will go to sleep after this time unless any of the A,B,X,Y, or DPAD buttons have been pressed
 *  Pressing Start button will then wake and restart
 */
#define SLEEP_TIMEOUT         600000  // 10 minutes

/* Reset Timeout
 *  Holding down the Select and then the Start button for this time will force the Open Gamepad to reset.
 */
#define RESET_TIMEOUT         10000  // 10 seconds

/*  How often we check the battery Voltage
*
*/
#define BATTERY_INTERVAL             10000  // 10 seconds

/* 
 *  How long we need for accumulate press
*
*/
#define ACCUMULATE_DELAY            2000  // 2 second


#define FLOW_CONSTANT               231.0f

/* NOSE I2C */
#define NOSE_SDA_PIN      NOSE_PIN_4
#define NOSE_SCL_PIN      NOSE_PIN_6


#define LPS33HW_A_ADDRESS      0x5c
#define LPS33HW_B_ADDRESS      0x5d

#define FLOW_CONSTANT               231.0f // convert differential pressure to flow

/* NOSE Analog  */
#define ADC_TO_PASCALS        2.5

/* Modes */

#define LOCAL_PRINT 
//#define PRESSURES_PLOT
#define UPDATE_PRESSURE_ZERO
/* end constant defines ************************************************************************/

/* variable declaration ************************************************************************/


volatile uint8_t    STSEL_buttons_state = 0;
volatile uint16_t   MCP_buttons_state = 0;
int8_t              HID_hat_state;
int16_t             left_x, left_y, right_x, right_y;
int16_t             left_x_zero, left_y_zero, right_x_zero, right_y_zero;
uint16_t            battery_voltage;
uint16_t            old_battery_level, new_battery_level;



volatile boolean    accumulate_pending = false;
volatile boolean    reset_pending     = false;
long                lasttimestepTime  = 0; 
long                currentTime       = 0; 
volatile long       sleepTime         = 0;
volatile long       resetTime         = 0;
volatile uint32_t   accumulateTime     = 0;

float pressure_A, pressure_B;             // pressure for sensors in pascals
float zero_pressure_A = 0, zero_pressure_B = 0;   // zero pressure value in pascals
float pressure_difference;
float flow_rate;                          // flow rate in ml/sec
float last_flow_rate;
boolean    volume_state; // true if we are accumulaing the volume from the flow
float      volume; // volume in ml
boolean  analog_sensor_OK = false;
boolean    LPS33HW_sensor_OK = false;
int16_t HID_pressure,HID_flow_rate, HID_volume;

BleGamepad OpenGamepadBLE("OpenGamepad", "Espressif", 100);
TwoWire  WireButtons = TwoWire(0);
TwoWire  WireSensor = TwoWire(1);
Adafruit_LPS35HW LPS33HW_A = Adafruit_LPS35HW(); 
Adafruit_LPS35HW LPS33HW_B = Adafruit_LPS35HW();
Adafruit_MCP23X17 MCP_Button_Pad;

/* end variable declaration ********************************************************************/


void IRAM_ATTR Start_Button_isr() {
  if(digitalRead(START_BUTTON_PIN) == 0)
  {
    OpenGamepadBLE.press(BUTTON_7);
    if(digitalRead(SELECT_BUTTON_PIN) == 0){
      reset_pending = true;
      resetTime = millis();
    }
  }
  else
  {
    OpenGamepadBLE.release(BUTTON_7);
    reset_pending = false;
  }
  resetTime = millis();
}

void IRAM_ATTR Select_Button_isr() {

  if(digitalRead(SELECT_BUTTON_PIN) == 0)
  {
    OpenGamepadBLE.press(BUTTON_8);
    accumulate_pending = true;
    accumulateTime = millis();   
  }
  else
  {
    OpenGamepadBLE.release(BUTTON_8);
    reset_pending = false;
    accumulate_pending = false;
  }
}

void IRAM_ATTR MCP23107_INTA_isr() {

  sleepTime = millis();
    
}


// Update pressure zero
void update_pressure_zero(void){
  if (pressure_A > 0.0f) zero_pressure_A += 0.01f;
  else if (pressure_A < 0.0f) zero_pressure_A -= 0.01f;

  if (pressure_B > 0.0f) zero_pressure_B += 0.01f;
  else if (pressure_B < 0.0f) zero_pressure_B -= 0.01f;
}



 /**
  * Print output data to serial
  */
void print_readings(){
  uint16_t mybuttons = 0;
  uint16_t buttonbit = 1;
  
  Serial.print(" Buttons: ");
  for(int8_t buttoncount = 0; buttoncount < MCP_NUM_BUTTONS; buttoncount++){
    if(OpenGamepadBLE.isPressed(buttoncount)) mybuttons |= buttonbit;
    buttonbit <<= 1;
  }
  
  Serial.print(mybuttons, HEX);
  Serial.print(" Hat: ");
  Serial.print(HID_hat_state, HEX);
  Serial.print(" Left ");
  Serial.print(left_x);
  Serial.print(" : ");
  Serial.print(left_y);
  Serial.print(" Right ");
  Serial.print(right_x);
  Serial.print(" : ");
  Serial.print(right_y);
  Serial.print(" Zzzz  ");
  Serial.print((currentTime-sleepTime)/1000);
  Serial.print(" Battery  ");
  Serial.println(battery_voltage);
}

/**
  * Print data to serial
  */
void plotpressures(){
    Serial.print(flow_rate);
    Serial.print(",");
    Serial.print(volume);
    Serial.print(",");
    Serial.print(pressure_A);
    Serial.print(",");
    Serial.println(pressure_B);

  
}

void check_battery(void){
  battery_voltage = map(analogRead(BATTERY_INPUT_PIN), 0, 4095, 0, 7100); // battery voltage in units of 10mV
  OpenGamepadBLE.setBatteryLevel((uint8_t) map(battery_voltage, 3200, 4200, 0, 100));
}

void get_joysticks(void){
  int16_t joystick_analog;
  
  joystick_analog = analogRead(JOYSTICK_L_H_PIN);
  if((joystick_analog - left_x_zero) >= 0) left_x = map(joystick_analog, left_x_zero , 4095, 0, 32767);
  else left_x = map(joystick_analog , 0, left_x_zero, -32767, 0);

  joystick_analog = analogRead(JOYSTICK_L_V_PIN);
  if((joystick_analog - left_x_zero) >= 0) left_y = map(joystick_analog, left_y_zero , 4095, 0, 32767);
  else left_y = map(joystick_analog , 0, left_y_zero, -32767, 0);

  joystick_analog = analogRead(JOYSTICK_R_H_PIN);
  if((joystick_analog - right_x_zero) >= 0) right_x = map(joystick_analog, right_x_zero , 4095, 0, 32767);
  else right_x = map(joystick_analog , 0, right_x_zero, -32767, 0);

  joystick_analog = analogRead(JOYSTICK_R_V_PIN);
  if((joystick_analog - right_y_zero) >= 0) right_y = map(joystick_analog, right_y_zero , 4095, 0, 32767);
  else right_y = map(joystick_analog , 0, right_y_zero, -32767, 0);
 
  
}

void sleep_mode(){
  /* Disable Bluetooth */
//  esp_bt_controller_disable();
  /* Enable RTC pullup on Start button */

  /* Wake on Start Button */
  esp_sleep_enable_ext0_wakeup(GPIO_NUM_15, LOW);

  /* Zzzzzz */
  esp_deep_sleep_start();
  
}

void map_buttons(void){
  if(MCP_buttons_state & MCP_BUTTON_A ) OpenGamepadBLE.press(BUTTON_1);
  else OpenGamepadBLE.release(BUTTON_1);
  if(MCP_buttons_state & MCP_BUTTON_B ) OpenGamepadBLE.press(BUTTON_2);
  else OpenGamepadBLE.release(BUTTON_2);
  if(MCP_buttons_state & MCP_BUTTON_X ) OpenGamepadBLE.press(BUTTON_3);
  else OpenGamepadBLE.release(BUTTON_3);
  if(MCP_buttons_state & MCP_BUTTON_Y ) OpenGamepadBLE.press(BUTTON_4);
  else OpenGamepadBLE.release(BUTTON_4);
  if(MCP_buttons_state & MCP_SHOULDER_L ) OpenGamepadBLE.press(BUTTON_5);
  else OpenGamepadBLE.release(BUTTON_5);
  if(MCP_buttons_state & MCP_SHOULDER_R ) OpenGamepadBLE.press(BUTTON_6);
  else OpenGamepadBLE.release(BUTTON_6);
  if(MCP_buttons_state & MCP_JOYSTICK_L ) OpenGamepadBLE.press(BUTTON_9);
  else OpenGamepadBLE.release(BUTTON_9);
  if(MCP_buttons_state & MCP_JOYSTICK_R ) OpenGamepadBLE.press(BUTTON_10);
  else OpenGamepadBLE.release(BUTTON_10);

  HID_hat_state = 0;
  if(MCP_buttons_state & MCP_DPAD_N ){
    if(MCP_buttons_state & MCP_DPAD_E ) HID_hat_state = DPAD_UP_RIGHT;
    else if(MCP_buttons_state & MCP_DPAD_W ) HID_hat_state = DPAD_UP_LEFT;
    else HID_hat_state = DPAD_UP;
  }
  
  
  if(MCP_buttons_state & MCP_DPAD_S ){
    if(MCP_buttons_state & MCP_DPAD_E ) HID_hat_state = DPAD_DOWN_RIGHT;
    else if(MCP_buttons_state & MCP_DPAD_W ) HID_hat_state = DPAD_DOWN_LEFT;
    else HID_hat_state = DPAD_DOWN;
  }
  

  if(!(MCP_buttons_state & MCP_DPAD_N) && !(MCP_buttons_state & MCP_DPAD_S)){
    if(MCP_buttons_state & MCP_DPAD_E ) HID_hat_state = DPAD_RIGHT;
    if(MCP_buttons_state & MCP_DPAD_W ) HID_hat_state = DPAD_LEFT;
  }
  
}

void setup() {
  Serial.begin(115200);

  

  // Set up the ESP32 GPIO Pins
  pinMode(MCP23017_RESET_PIN, OUTPUT);
  digitalWrite(MCP23017_RESET_PIN, LOW);
  digitalWrite(MCP23017_RESET_PIN, HIGH);
  
  pinMode(MCP23017_INTA_PIN, INPUT_PULLUP);
  pinMode(START_BUTTON_PIN, INPUT_PULLUP);
  pinMode(SELECT_BUTTON_PIN, INPUT_PULLUP);

  pinMode(NOSE_PIN_1, INPUT_PULLUP);
  pinMode(NOSE_PIN_3, INPUT_PULLUP);
  pinMode(NOSE_PIN_4, INPUT_PULLUP);
  pinMode(NOSE_PIN_5, INPUT_PULLUP);
  pinMode(NOSE_PIN_6, INPUT_PULLUP);
  pinMode(NOSE_PIN_7, INPUT_PULLUP);
  
  pinMode(STATUS_LED_PIN, OUTPUT);
  digitalWrite(STATUS_LED_PIN, LOW);

  // Setup the MPC23017 Button Controller
  WireButtons.begin (MCP23017_SDA_PIN,MCP23017_SCL_PIN);   // 
  MCP_Button_Pad.begin_I2C( MCP23017_I2C_ADDR, &WireButtons);
  
  MCP_Button_Pad.setupInterrupts(true,false,LOW); // single mirrored interrupt going low

  for(int8_t MCP_Button = 0; MCP_Button < MCP_NUM_BUTTONS; MCP_Button++){
    MCP_Button_Pad.pinMode(MCP_Button, INPUT_PULLUP);
    MCP_Button_Pad.setupInterruptPin(MCP_Button, CHANGE);
  }

  
  // Test if Analog Nose (NOSE_PIN_6 is low)

  if(digitalRead(NOSE_PIN_6)) LPS33HW_sensor_OK = true;
  else{
    analog_sensor_OK = true;
    pressure_A = analogRead(NOSE_PIN_7);
    zero_pressure_A = pressure_A;
  }
  
  // Interrupts for the  Buttons
  attachInterrupt(START_BUTTON_PIN, Start_Button_isr, CHANGE);
  attachInterrupt(SELECT_BUTTON_PIN, Select_Button_isr, CHANGE);
  attachInterrupt(MCP23017_INTA_PIN, MCP23107_INTA_isr, FALLING);

  sleepTime = millis();

  if(!analog_sensor_OK){ // only try to detect BMP280 if not analog sensor
  
      // Set up the LPS33HW pressure Sensors
    WireSensor.begin (NOSE_SDA_PIN, NOSE_SCL_PIN);   // 
  
    if (!LPS33HW_A.begin_I2C(LPS33HW_A_ADDRESS, &WireSensor)) {
      Serial.println(" LPS33HW Sensor A fail");
      LPS33HW_sensor_OK = false;
    }
    if (!LPS33HW_B.begin_I2C(LPS33HW_B_ADDRESS, &WireSensor)) {
      Serial.println(" LPS33HW Sensor B fail");
      LPS33HW_sensor_OK = false;
    }
  }

  if( LPS33HW_sensor_OK) {
    // Initialise the zero

      
      for (int i =0; i < 100; i++){
        zero_pressure_A += LPS33HW_A.readPressure();
        zero_pressure_B += LPS33HW_B.readPressure();
        //Serial.println(zero_pressure_B);
        delay(10);
      }
      
      // One shot measurement mode 
      LPS33HW_A.setDataRate(LPS35HW_RATE_ONE_SHOT);
      LPS33HW_B.setDataRate(LPS35HW_RATE_ONE_SHOT);
  }
  
 // Get the zero points for the Joysticks
 for(int8_t rcount = 0; rcount < 16; rcount ++) left_x_zero += analogRead(JOYSTICK_L_H_PIN);
 left_x_zero = left_x_zero / 16;
 for(int8_t rcount = 0; rcount < 16; rcount ++) left_y_zero += analogRead(JOYSTICK_L_V_PIN);
 left_y_zero = left_y_zero / 16;
 for(int8_t rcount = 0; rcount < 16; rcount ++) right_x_zero += analogRead(JOYSTICK_R_H_PIN);
 right_x_zero = right_x_zero / 16;
 for(int8_t rcount = 0; rcount < 16; rcount ++) right_y_zero += analogRead(JOYSTICK_R_V_PIN);
 right_y_zero = right_y_zero / 16; 

  Serial.print(" Left ");
  Serial.print(left_x_zero);
  Serial.print(" : ");
  Serial.print(left_y_zero);
  Serial.print(" Right ");
  Serial.print(right_x_zero);
  Serial.print(" : ");
  Serial.println(right_y_zero);
  
  // Start the bluetooth

  OpenGamepadBLE.begin();
  OpenGamepadBLE.setAutoReport(false);

}

void loop() {
  currentTime = millis();
  if(currentTime-lasttimestepTime > TIMESTEP){
     if(reset_pending && (currentTime-resetTime > RESET_TIMEOUT)){
        ESP.restart();
     }
     if(accumulate_pending && (currentTime-accumulateTime > ACCUMULATE_DELAY)){
        accumulate_pending = false;
        if(volume_state == false) {
          volume_state = true;         
          OpenGamepadBLE.press(BUTTON_11);         
          volume = 0;
        }    
       else {
          volume_state = false;         
          OpenGamepadBLE.release(BUTTON_11);
          

       }
    }
     
     if(currentTime-sleepTime > SLEEP_TIMEOUT) sleep_mode();
     
     check_battery();
     MCP_buttons_state = ~MCP_Button_Pad.readGPIOAB(); // invert the pins
     map_buttons();
     get_joysticks();
     
  if( LPS33HW_sensor_OK) {
     pressure_A = LPS33HW_A.readPressure() * 100; // pressure in pa
     pressure_A -= zero_pressure_A;
     pressure_A = constrain(pressure_A, -10000.0f, 10000.0f); // limit to +/- 10kpa

     pressure_B = LPS33HW_B.readPressure() * 100; // pressure in pa
     pressure_B -= zero_pressure_B;
     pressure_B = constrain(pressure_B, -10000.0f, 10000.0f); // limit to +/- 10kpa
     
     pressure_difference = pressure_A - pressure_B; // get the differential venturi pressure
     pressure_difference = abs(pressure_difference);  // must be +ve for sqrt

     if(pressure_A > 0.0f) flow_rate = FLOW_CONSTANT * sqrt(pressure_difference); // Exhale +ve ml/s
     else flow_rate = -FLOW_CONSTANT * sqrt(pressure_difference); // Inhale -ve ml/s
     flow_rate = constrain(flow_rate, -10000.0f, 10000.0f);
     flow_rate = (last_flow_rate * 0.8) + (flow_rate *0.2); // exponential filter
     last_flow_rate = flow_rate; 
     if(volume_state == true)volume += flow_rate * TIMESTEP / 1000; //volume in ml
     else volume = 0;
     volume =  constrain(volume, -10000.0f, 10000.0f);

     LPS33HW_A.takeMeasurement();
     LPS33HW_B.takeMeasurement();
  
     HID_pressure = (int16_t) map(pressure_A, -10000, 10000, -32767, 32767);
     HID_flow_rate = (int16_t) map(flow_rate, -10000, 10000, -32767, 32767);
     HID_volume = (int16_t) map(volume, -10000, 10000, -32767, 32767);
  }

  if(analog_sensor_OK){
    float ADC_pressure = (float) analogRead(NOSE_PIN_7)* ADC_TO_PASCALS ;
    pressure_A =  ADC_pressure  - zero_pressure_A; // offset zero
  }
  
#ifdef PRESSURES_PLOT
     if(LPS33HW_sensor_OK || analog_sensor_OK) plotpressures();
#endif
  
#ifdef LOCAL_PRINT
    print_readings();
#endif

#ifdef UPDATE_PRESSURE_ZERO
    update_pressure_zero();
#endif


    if(OpenGamepadBLE.isConnected()) {
      
     OpenGamepadBLE.setAxes(left_x, left_y, HID_flow_rate, HID_pressure, right_x, right_y, HID_volume, 0, HID_hat_state, HAT_CENTERED, HAT_CENTERED, HAT_CENTERED);
     OpenGamepadBLE.sendReport();
        
  
    }
    lasttimestepTime = currentTime;
  }

}
