#ifndef ESP32_BLE_GAMEPAD_H
#define ESP32_BLE_GAMEPAD_H
#include "sdkconfig.h"
#if defined(CONFIG_BT_ENABLED)

#include "BleGamepadStatus.h"
#include "BLEHIDDevice.h"
#include "BLECharacteristic.h"

#define BUTTON_1 	0       // A button
#define BUTTON_2 	1
#define BUTTON_3 	2
#define BUTTON_4  3
#define BUTTON_5  4
#define BUTTON_6  5
#define BUTTON_7  6
#define BUTTON_8  7
#define BUTTON_9  8
#define BUTTON_10  9
#define BUTTON_11  10
#define BUTTON_12  11
#define BUTTON_13  12
#define BUTTON_14  13

#define DPAD_CENTERED   0
#define DPAD_UP     1
#define DPAD_UP_RIGHT   2
#define DPAD_RIGHT    3
#define DPAD_DOWN_RIGHT 4
#define DPAD_DOWN     5
#define DPAD_DOWN_LEFT  6
#define DPAD_LEFT     7
#define DPAD_UP_LEFT  8


class BleGamepad {
private:
  uint16_t _buttons;
  BleConnectionStatus* connectionStatus;
  BLEHIDDevice* hid;
  BLECharacteristic* inputGamepad;
  
  static void taskServer(void* pvParameter);
public:
  BleGamepad(std::string deviceName = "Open Gamepad", std::string deviceManufacturer = "Breathing Games", uint8_t batteryLevel = 100);
  void begin(void);
  void end(void);
  void setAxes(signed char x, signed char y, signed char z = 0, signed char rZ = 0, char rX = 0, char rY = 0, signed char hat = 0);

  void buttons(uint16_t b);
  
  bool isConnected(void);
  void setBatteryLevel(uint8_t level);
  uint8_t batteryLevel;
  std::string deviceManufacturer;
  std::string deviceName;
protected:
  virtual void onStarted(BLEServer *pServer) { };
};

#endif // CONFIG_BT_ENABLED
#endif // ESP32_BLE_GAMEPAD_H
