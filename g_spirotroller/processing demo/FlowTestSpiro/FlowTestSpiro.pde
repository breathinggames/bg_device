import processing.serial.*;

  Serial myPort;        // The serial port
  int xPos = 1;         // horizontal position of the graph
  float inByte0 = 0;
  float inByte1 = 0;
  float inByte2 = 0;

  void setup () {
    // set the window size:
    size(1500 , 750);

    // List all the available serial ports
    // if using Processing 2.1 or later, use Serial.printArray()
    println(Serial.list());

    // I know that the first port in the serial list on my Mac is always my
    // Arduino, so I open Serial.list()[0].
    // Open whatever port is the one you're using.
    myPort = new Serial(this, Serial.list()[0], 9600);

    // don't generate a serialEvent() unless you get a newline character:
    myPort.bufferUntil('\n');

    // set initial background:
    background(0);
  }

  int scaler = 1; 
  void draw () {
    // draw the line:
    strokeWeight(scaler);
    stroke(127, 34, 255);
    //line(xPos, height, xPos, (height - 1*inByte0));//volume
     line(xPos, height, xPos, (height - 1*inByte1));//flow rate
    
    String s1 = "Flow Rate: " + String.valueOf(inByte1);
    String s2 = "Volume of Last: " + String.valueOf(inByte2);
    
    fill(0); 
    stroke(0);
    rect(50,30,500,180); 
    
    fill(255);
    textSize(40); 
    text(s1,100,100); 
    text(s2,100,150); 

    // at the edge of the screen, go back to the beginning:
    if (xPos >= width) {
      xPos = 0;
      background(0);
    } else { //<>//
      // increment the horizontal position:
      xPos= xPos+scaler;
    }
  }
 //<>//
  void serialEvent (Serial myPort) {
    // get the ASCII string:
    String inString = myPort.readStringUntil('\n');
    String[] input = inString.split(";"); 
  
    if (input[0] != null && input[1] != null && input[2] != null) {
      // trim off any whitespace: 
      input[0] = trim(input[0]); //<>//
      input[1] = trim(input[1]);
      input[2] = trim(input[2]);
 
      inByte0 = float( input[0]);
      inByte1 = float( input[1]);
      inByte2 = float( input[2]);
      
      // convert to an int and map to the screen height:
      println(inByte0);
      inByte0 = map(inByte0, 0, 20, 0, height);
      
    }
  }
