These parts are meant to be printed with no support material. 

We recommend printing with Taulman Gu!deline filament at 250C with a heated bed at 60C. 

Our prints were performed on a Prusa MK3, the print settings used can be found in the attached Slic3r_config_bundle. 

