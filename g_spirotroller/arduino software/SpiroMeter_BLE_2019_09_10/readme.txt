Notes on firmware version:


Data output:
	
Output of data for both bluetooth and USB serial follows the following format:

volume(liters);flowrate(liters/minute);volumeRecorded(liters)



Volume recording:

The bottom button needs to be pressed down to transmit data over bluetooth as well as record the volumeRecorded value.



Bluetooth reset:

Press the top button of the device to reset the bluetooth module



Bluetooth device name change:

To change bluetooth device name modify line:

#define DEVICE_NAME	"Breathing Games 2"

To the desired name:

#define DEVICE_NAME	"your device name here"

in constant defines section and reload firmware



Turning off bluetooth module

To turn off bluetooth module modify line:

#define BLE		1

To:

#define BLE		0

in constant defines section and reload firmware



Follow Adafruit tutorial for issues and troubleshoots to loading arduino project firmware:
https://learn.adafruit.com/adafruit-feather-32u4-bluefruit-le/setup



