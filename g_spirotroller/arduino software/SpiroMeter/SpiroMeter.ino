/**
 ************************************************************************************************
 * @file       SpiroMeter.ino
 * @author     Colin Gallacher, Steve Ding
 * @version    V1.0.0
 * @date       10-December-2018
 * @brief      Adafruit Feather 32u4 Bluefruit LE Spirometer control code for flow rate 
 *             recording 
 ************************************************************************************************
 * @attention
 *
 *
 ************************************************************************************************
 */
 
/* includes ************************************************************************************/
#include <MegunoLink.h>
#include <Filter.h>
/* end includes ********************************************************************************/


/* constant defines ****************************************************************************/
#define   TIMESTEP            50

#define   ANALOG_IN           A0
#define   RECORD_BUTTON       12
#define   RECORD_LED          11

#define   AREA1               0.001 //m^2
#define   AREA2               0.0005 //m^2
#define   AIR_DENSITY         1.225 //kg/m^3
#define   DENOMINATOR()       (((AREA1/AREA2) * (AREA1/AREA2)) - 1)

#define   OFFSET              69
#define   CORRECTIONFACTOR    0.15
/* end constant defines ************************************************************************/


/* variable declaration ************************************************************************/
long      lastTime            = 0; 
long      currentTime         = 0; 

boolean   wasRecording        = false; 

float     oldPressureDiffKPA  = 0.0f;
float     pressureDiffKPA;
float     flowRate;
 
float     volume              = 0.0f; 
float     volumeRecorded      = 0.0f; 

ExponentialFilter<long> ADCFilter(60, 0);
/* end variable declaration ********************************************************************/


/* pressure calculation functions **************************************************************/
 /**
  * Calculate pressure difference in KPA based on analog signal input
  *
  * @param    analogPin analog input pin
  * @return   pressure difference in KPA
  */
float pressure_difference_kpa(int analogPin){
  
  int diff = analogRead(analogPin);
  
  ADCFilter.Filter(diff);
  
  float offsetMv = OFFSET * 3.2; //10bit;
  float diff2mv = ADCFilter.Current() * 3.2;
  float pressureDiff = (diff2mv - offsetMv) / 450; //kpa

  pressureDiff = (pressureDiff < .1) ? 0 : pressureDiff;

  return pressureDiff;
}


 /**
  * Calculate flow rate based on recorded pressure
  *
  * @param    pressure pressure in KPA
  * @return   flow rate
  */
float get_flow_rate(float pressure){
  
  float numerator = 2 * pressure * 1000 / AIR_DENSITY;
  float flow = AREA1 * sqrt(numerator/DENOMINATOR());

  return flow;
}
/* end pressure calculation functions **********************************************************/


/* recording and display functions *************************************************************/

 /**
  * Record volume readings based on button click
  *
  * @param    record_button button pin to initialize recording
  */
void record_reading(int record_button){

  switch(!digitalRead(record_button)){
    case 0:
      digitalWrite(RECORD_LED, LOW);
      if(wasRecording){
        volumeRecorded = volume;
        volume = 0.0f;
      }
      wasRecording = false;
      break;
      
    case 1:
      volume = volume + flowRate * TIMESTEP / 1000;
      digitalWrite(RECORD_LED, HIGH);
      wasRecording = true;
      break; 
  }
}


 /**
  * Print data to serial
  */
void print_readings(){
  Serial.print(volume * 1000 * CORRECTIONFACTOR); //m^3 to liters
  Serial.print(";");
  Serial.print(flowRate * 60000 * CORRECTIONFACTOR);//m^3/s to liters/minute
  Serial.print(";");
  Serial.println(volumeRecorded);
}
/* end recording and display functions *********************************************************/



/***********************************************************************************************
 * Main setup function, defines parameters and hardware setup
 ***********************************************************************************************/
void setup() {
  Serial.begin(9600); 
  
  pinMode(RECORD_BUTTON, INPUT_PULLUP); 
  pinMode(RECORD_LED, OUTPUT);   
}
/***********************************************************************************************
 * End main setup function, defines parameters and hardware setup
 ***********************************************************************************************/



/***********************************************************************************************
 * Main loop function
 ***********************************************************************************************/
void loop() {
  
  currentTime = millis();

  if(currentTime-lastTime > TIMESTEP){
    
    pressureDiffKPA = pressure_difference_kpa(ANALOG_IN);

    flowRate = get_flow_rate(pressureDiffKPA);
    
    record_reading(RECORD_BUTTON);

    oldPressureDiffKPA = pressureDiffKPA;

    print_readings();

    
    lastTime = currentTime;
  }
}
/***********************************************************************************************
 * End main loop function
 ***********************************************************************************************/

