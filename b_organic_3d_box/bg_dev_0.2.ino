/************************************************************
 *                       Breathing Games                    *
 *                                                          *
 *           Peripheral device sketch on Arduino Nano       *
 *             written by Jim Anastassiou     v 0.2         *
 ************************************************************/

int sensorPin = A4;     // select the input pin for the pressure sensor
int sensorValue = 0;    // variable to store the value coming from the sensor
unsigned int scaledPressure = 0; // the output of the device from 1 - 100
int pinBrightness = 0;  // pin brightness variable
int led = 9;            // the pin that the LED is attached to

void setup()
{
  Serial.begin(9600);  
  pinMode(led, OUTPUT);      // sets the digital pin as output
}

void loop() {
  
// read the value from the sensor:
sensorValue = analogRead(sensorPin); 
scaledPressure = constrain(map(sensorValue,520,590,0,100),0,100);
//Serial.println(sensorValue);
//Serial.print(",");
Serial.println(scaledPressure);
//Serial.print(",");
//Serial.println(pinBrightness);
switch (scaledPressure){
  case 0 ... 10:
    pinBrightness = 0;
    break;
  case 11 ... 25:
    pinBrightness = 65;
    break;
  case 26 ... 50:
    pinBrightness = 130;
    break;
  case 51 ... 75:
    pinBrightness = 195;
    break;
  case 76 ... 100:
    pinBrightness = 255;
    break;
  }
analogWrite(led,pinBrightness); 
delay(10);

}
