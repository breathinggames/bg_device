/************************************************************
 *                       Breathing Games                    *
 *                                                          *
 *           Peripheral device sketch on Arduino Nano       *
 *             written by Jim Anastassiou     v 0.5         *
 ************************************************************/

int sensorPin = A5;     // select the input pin for the pressure sensor
int sensorValue = 0;    // variable to store the value coming from the sensor
int scaledPressure = 0; // output re-mapped from 0 to 100 for postitve pressure & 0 to -100 for negative
int pinBrightness = 0;  // pin brightness variable
int ledPin = 9;         // LED pin for pressure
void setup()
{
  Serial.begin(9600);  
  pinMode(ledPin, OUTPUT);      // sets the digital pin as output
}

void loop() {
  
// read the value from the sensor:
sensorValue = analogRead(sensorPin); 

//re-map positive pressure from 0 to 100
if (sensorValue >= 270) {
    scaledPressure = constrain(map(sensorValue,270,820,0,100),0,100);  
    }
//re-map negative pressure rom 0 to -100
else if (sensorValue <= 260){
    scaledPressure = constrain(map(sensorValue,260,150,0,-100),0,-100);
}

//Serial.print(sensorValue);
//Serial.print(",");
Serial.println(scaledPressure);
//Serial.print(",");
//Serial.println(pinBrightness);
switch (scaledPressure){
  case 0 ... 10:
    pinBrightness = 0;
    break;
  case 11 ... 25:
    pinBrightness = 15;
    break;
  case 26 ... 50:
    pinBrightness = 50;
    break;
  case 51 ... 75:
    pinBrightness = 120;
    break;
  case 76 ... 100:
    pinBrightness = 255;
    break;
  }
analogWrite(ledPin,pinBrightness); 
delay(10);

}
