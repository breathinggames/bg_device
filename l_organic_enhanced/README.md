# PEP Master pressure sensor
![PEP Master pressure sensor](Photos/20230515_194516.jpg)

## Goals
Create hardware interfaces to serious games used in healthcare. This particular R&D effort is to create a PEP (Positive expiratory pressure) interface, which can be used as a controller for a videogame, used in cystic fibrosis therapy. In essence, the goal is to design and prototype a hardware device that can sense air pressure and communicate that to a computer or a mobile device that hosts a video game. 

## Organizational environment
The PEP Master pressure sensor is developed by the Sensorica network, in collaboration with Breathing Games network. The pressure sensor is designed to work with the PEP Master games. 
